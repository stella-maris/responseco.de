# Error-Server

A test-server that returns the requested Status-Code

A call to https://responseco.de/{code} will return a {code} response-code.

So requesting https://responseco.de/404 will return a 404 response, 
requesting https://responseco.de/500 will return a 500 response and https://responseco.de/418 will respond with the famous "I am a teapot".

The 30x responses will contain a Location header to the 200 resource and the 401 response will 
contain an Authorization-header

Apart from that it's just the plain header and a response-body stating the Response-text.