<?php
/**
 * Copyright Andreas Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */
$code = (int) trim($_REQUEST['code'], '/');
//var_dump($_REQUEST);exit;

const CODES = [
    100 => ['code' => 'Continue'],
    101 => ['code' => 'Switching Protocols', 'headers' => ['Upgrade: websocket', 'Connection: Upgrade']],
    102 => ['code' => 'Processing'],
    103 => ['code' => 'Early Hints'],
    200 => ['code' => 'OK'],
    201 => ['code' => 'Created'],
    202 => ['code' => 'Accepted'],
    203 => ['code' => 'Non-Authoritative Information'],
    204 => ['code' => 'No Content'],
    205 => ['code' => 'Reset Content'],
    206 => ['code' => 'Partial Content'],
    208 => ['code' => 'Already reported'],
    226 => ['code' => 'IM Used'],
    300 => ['code' => 'Multiple Choices'],
    301 => ['code' => 'Moved Permanently', 'headers' => ['Location: https://responseco.de/200']],
    302 => ['code' => 'Found', 'headers' => ['Location: https://responseco.de/200']],
    303 => ['code' => 'See Other', 'headers' => ['Location: https://responseco.de/200']],
    304 => ['code' => 'Not Modified'],
    305 => ['code' => 'Use Proxy'],
    306 => ['code' => 'unused'],
    307 => ['code' => 'Temporary Redirect', 'headers' => ['Location: https://responseco.de/200']],
    308 => ['code' => 'Permanent Redirect', 'headers' => ['Location: https://responseco.de/200']],
    400 => ['code' => 'Bad Request'],
    401 => ['code' => 'Unauthorized', 'headers' => ['WWW-Authenticate: Basic realm="Login"']],
    402 => ['code' => 'Payment Required'],
    403 => ['code' => 'Forbidden'],
    404 => ['code' => 'Not Found'],
    405 => ['code' => 'Method Not Allowed'],
    406 => ['code' => 'Not Acceptable'],
    407 => ['code' => 'Proxy Authentication Required'],
    408 => ['code' => 'Request Timeout'],
    409 => ['code' => 'Conflict'],
    410 => ['code' => 'Gone'],
    411 => ['code' => 'Length Required'],
    412 => ['code' => 'Precondition Failed'],
    413 => ['code' => 'Payload Too Large'],
    414 => ['code' => 'URI Too Long'],
    415 => ['code' => 'Unsupported Media Type'],
    416 => ['code' => 'Requested Range Not Satisfiable'],
    417 => ['code' => 'Expectation Failed'],
    418 => ['code' => 'I Am A Teapot'],
    421 => ['code' => 'Misdirected Request'],
    426 => ['code' => 'Upgrade Required'],
    428 => ['code' => 'Precondition Required'],
    429 => ['code' => 'Too Many Requests'],
    431 => ['code' => 'Request Header Fields Too Large'],
    451 => ['code' => 'Unavailable For Legal Reasons'],
    500 => ['code' => 'Internal Server Error'],
    501 => ['code' => 'Not Implemented'],
    502 => ['code' => 'Bad Gateway'],
    503 => ['code' => 'Service Unavailable'],
    504 => ['code' => 'Gateway Timeout'],
    505 => ['code' => 'HTTP Version Not Supported'],
    506 => ['code' => 'Variant Also Negotiates'],
    507 => ['code' => 'Insufficient Storage'],
    508 => ['code' => 'Loop Detected'],
    510 => ['code' => 'Not Extended'],
    511 => ['code' => 'Network Authentication Required'],
];

header(sprintf(
    'HTTP/1.1 %1$d %2$s',
    $code,
    CODES[$code]['code']
));

if (isset(CODES[$code]['headers'])) {
    foreach (CODES[$code]['headers'] as $header) {
        header($header);
    }
}

header('Content-Type: text/plain');

echo CODES[$code]['code'];
